

//Just device names
//ioreg -p IOUSB -w0 | sed 's/[^o]*o //; s/@.*$//' | grep -v '^Root.*'

//Diff view
//ioreg -p IOUSB -w0 -l

//view 1 

//ioreg -p IOUSB

const str = `      | +-o Razer Abyssus V2@14623000  <class AppleUSBDevice, id 0x100009850, registered, matched, active, busy 0 (632 ms), retain 18>
|     {
|       "sessionID" = 94416044139401
|       "iManufacturer" = 1
|       "bNumConfigurations" = 1
|       "idProduct" = 91
|       "bcdDevice" = 512
|       "Bus Power Available" = 250
|       "USB Address" = 25
|       "bMaxPacketSize0" = 64
|       "iProduct" = 2
|       "iSerialNumber" = 0
|       "bDeviceClass" = 0
|       "Built-In" = No
|       "locationID" = 341979136
|       "bDeviceSubClass" = 0
|       "bcdUSB" = 512
|       "USB Product Name" = "Razer Abyssus V2"
|       "PortNum" = 3
|       "non-removable" = "no"
|       "IOCFPlugInTypes" = {"9dc7b780-9ec0-11d4-a54f-000a27052861"="IOUSBFamily.kext/Contents/PlugIns/IOUSBLib.bundle"}
|       "bDeviceProtocol" = 0
|       "IOUserClientClass" = "IOUSBDeviceUserClientV2"
|       "IOPowerManagement" = {"DevicePowerState"=0,"CurrentPowerState"=3,"CapabilityFlags"=65536,"MaxPowerState"=4,"DriverPowerState"=3}
|       "kUSBCurrentConfiguration" = 1
|       "Device Speed" = 1
|       "USB Vendor Name" = "Razer"
|       "idVendor" = 5426
|       "IOGeneralInterest" = "IOCommand is not serializable"
|       "IOClassNameOverride" = "IOUSBDevice"
|     }
|
+-o USB 2.0 Hub@14640000  <class AppleUSBDevice, id 0x100009827, registered, matched, active, busy 0 (707 ms), retain 16>
  | {
  |   "sessionID" = 94415837545817
  |   "iManufacturer" = 0
  |   "bNumConfigurations" = 1
  |   "idProduct" = 2049
  |   "bcdDevice" = 256
  |   "Bus Power Available" = 250
  |   "USB Address" = 23
  |   "bMaxPacketSize0" = 64
  |   "iProduct" = 1
  |   "iSerialNumber" = 0
  |   "bDeviceClass" = 9
  |   "Built-In" = No
  |   "locationID" = 342097920
  |   "bDeviceSubClass" = 0
  |   "bcdUSB" = 512
  |   "USB Product Name" = "USB 2.0 Hub"
  |   "PortNum" = 4
  |   "non-removable" = "no"
  |   "IOCFPlugInTypes" = {"9dc7b780-9ec0-11d4-a54f-000a27052861"="IOUSBFamily.kext/Contents/PlugIns/IOUSBLib.bundle"}
  |   "bDeviceProtocol" = 1
  |   "IOUserClientClass" = "IOUSBDeviceUserClientV2"
  |   "IOPowerManagement" = {"DevicePowerState"=0,"CurrentPowerState"=3,"CapabilityFlags"=65536,"MaxPowerState"=4,"DriverPowerState"=3}
  |   "kUSBCurrentConfiguration" = 1
  |   "Device Speed" = 2
  |   "idVendor" = 6720
  |   "IOGeneralInterest" = "IOCommand is not serializable"
  |   "IOClassNameOverride" = "IOUSBDevice"
  | }
  |
  +-o USB 2.0 BILLBOARD             @14644000  <class AppleUSBDevice, id 0x100009a01, registered, matched, active, busy 0 (711 ms), retain 14>
  |   {
  |     "sessionID" = 94418171772222
  |     "iManufacturer" = 1
  |     "bNumConfigurations" = 1
  |     "idProduct" = 256
  |     "bcdDevice" = 513
  |     "Bus Power Available" = 250
  |     "USB Address" = 28
  |     "bMaxPacketSize0" = 64
  |     "iProduct" = 2
  |     "iSerialNumber" = 3
  |     "bDeviceClass" = 17
  |     "Built-In" = No
  |     "locationID" = 342114304
  |     "bDeviceSubClass" = 0
  |     "bcdUSB" = 513
  |     "USB Product Name" = "USB 2.0 BILLBOARD             "
  |     "PortNum" = 4
  |     "non-removable" = "no"
  |     "IOCFPlugInTypes" = {"9dc7b780-9ec0-11d4-a54f-000a27052861"="IOUSBFamily.kext/Contents/PlugIns/IOUSBLib.bundle"}
  |     "bDeviceProtocol" = 0
  |     "IOUserClientClass" = "IOUSBDeviceUserClientV2"
  |     "IOPowerManagement" = {"DevicePowerState"=0,"CurrentPowerState"=3,"CapabilityFlags"=65536,"MaxPowerState"=4,"DriverPowerState"=3}
  |     "kUSBCurrentConfiguration" = 1
  |     "Device Speed" = 2
  |     "USB Vendor Name" = "VIA Technologies Inc.         "
  |     "idVendor" = 8457
  |     "IOGeneralInterest" = "IOCommand is not serializable"
  |     "USB Serial Number" = "0000000000000001"
  |     "IOClassNameOverride" = "IOUSBDevice"
  |   }
  |
  +-o VLI Product String@14643000  <class AppleUSBDevice, id 0x100009a20, registered, matched, active, busy 0 (9 ms), retain 14>
      {
        "sessionID" = 94424839152794
        "iManufacturer" = 1
        "bNumConfigurations" = 1
        "idProduct" = 1813
        "bcdDevice" = 0
        "Bus Power Available" = 250
        "USB Address" = 29
        "bMaxPacketSize0" = 64
        "iProduct" = 2
        "iSerialNumber" = 3
        "bDeviceClass" = 0
        "Built-In" = No
        "locationID" = 342110208
        "bDeviceSubClass" = 0
        "bcdUSB" = 528
        "USB Product Name" = "VLI Product String"
        "PortNum" = 3
        "non-removable" = "no"
        "IOCFPlugInTypes" = {"9dc7b780-9ec0-11d4-a54f-000a27052861"="IOUSBFamily.kext/Contents/PlugIns/IOUSBLib.bundle"}
        "bDeviceProtocol" = 0
        "IOUserClientClass" = "IOUSBDeviceUserClientV2"
        "IOPowerManagement" = {"DevicePowerState"=0,"CurrentPowerState"=3,"CapabilityFlags"=65536,"MaxPowerState"=4,"DriverPowerState"=3}
        "kUSBCurrentConfiguration" = 1
        "Device Speed" = 2
        "USB Vendor Name" = "VLI Manufacture String"
        "idVendor" = 8457
        "uid" = "USB:21090715000000123DE9"
        "IOGeneralInterest" = "IOCommand is not serializable"
        "USB Serial Number" = "000000123DE9"
        "IOClassNameOverride" = "IOUSBDevice"
      }
`;


const cmds = [
  "ioreg -p IOUSB -w0 | sed 's/[^o]*o //; s/@.*$//' | grep -v '^Root.*'",
  "ioreg -p IOUSB -w0 -l",
  "ioreg -p IOUSB",
  "system_profiler SPUSBDataType"
];

var getFromBetween = {
  results:[],
  string:"",
  getFromBetween:function (sub1,sub2) {
      if(this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
      var SP = this.string.indexOf(sub1)+sub1.length;
      var string1 = this.string.substr(0,SP);
      var string2 = this.string.substr(SP);
      var TP = string1.length + string2.indexOf(sub2);
      return this.string.substring(SP,TP);
  },
  removeFromBetween:function (sub1,sub2) {
      if(this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
      var removal = sub1+this.getFromBetween(sub1,sub2)+sub2;
      this.string = this.string.replace(removal,"");
  },
  getAllResults:function (sub1,sub2) {
      // first check to see if we do have both substrings
      if(this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return;

      // find one result
      var result = this.getFromBetween(sub1,sub2);
      // push it to the results array
      this.results.push(result);
      // remove the most recently found one from the string
      this.removeFromBetween(sub1,sub2);

      // if there's more substrings
      if(this.string.indexOf(sub1) > -1 && this.string.indexOf(sub2) > -1) {
          this.getAllResults(sub1,sub2);
      }
      else return;
  },
  get:function (string,sub1,sub2) {
      this.results = [];
      this.string = string;
      this.getAllResults(sub1,sub2);
      return this.results;
  }
};


async function parse1() {
  var result = getFromBetween.get(str,"{","}");
  console.log(result);
}


function doExec(cmd) {
  const exec = require('child_process').exec;
  return new Promise((resolve, reject) => {
   exec(cmd, (error, stdout, stderr) => {
    if (error) {
     console.warn(error);
    }
    resolve(stdout? stdout : stderr);
   });
  });
 }

async function main() {
 // let ret = await doExec(cmds[0]);
 // console.log(ret);
 await  parse1();
}
main();